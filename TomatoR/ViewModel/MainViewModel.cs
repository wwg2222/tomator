﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Timers;
using System.Windows.Input;
using TomatoR.Model;

namespace TomatoR.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged(WelcomeTitlePropertyName);
            }
        }

        private Timer timer;


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });
            timer = new Timer(1000);
            timer.Elapsed += new ElapsedEventHandler((s, e) => RaisePropertyChanged("Timestamp"));
            timer.Start();
        }

        public ICommand StartNextActionCommand
        {
            get
            {
                return new RelayCommand(execute: () =>
                        {
                            if (true)// NextActon == TomatoAction)
                            { }
                            else
                            { }
                        },
                    canExecute: () => true);
            }
        }


        public ICommand StopCurrentCommand
        { 
            get
            {
                return new RelayCommand(execute: () =>
                        {
                            if (true)// NextActon == TomatoAction)
                            { }
                            else
                            { }
                        },
                    canExecute: () => true);
            }
        }

        public string Timestamp
        {
            get { return DateTime.Now.ToLongTimeString(); }
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}